const express = require('express');								// using express
const bodyParser = require('body-parser');						// parsing incoming request in middleware b4 we handle it
const pino = require('express-pino-logger')();					// allows for effortless logging
const client = require('twilio')(								// using twilio
	process.env.TWILIO_ACCOUNT_SID,								// if no credentials are provided when instantiating the 
	process.env.TWILIO_AUTH_TOKEN								// Twilio client (like this example ('twilio')();) then use TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN
	);

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(pino);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(pino);

app.get('/api/greeting', (req, res) => {
  const name = req.query.name || 'World';						// https://www.educative.io/edpresso/what-is-reqquery-in-expressjs
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ greeting: `Hello ${name}!` }));
});

app.post('/api/messages', (req, res) => {
	res.header('Content-Type', 'application/json');
	client.messages
	.create({
		from: process.env.TWILIO_PHONE_NUMBER,
		to: req.body.to,
		body: req.body.body
	})
	.then(() => {
		res.send(JSON.stringify({ success: true }));
	})
	.catch(err => {
		console.log(err);
		res.send(JSON.stringify({ success: false}));
	})
});
app.listen(3001, () =>							// listens on this port for connections, port 3001... app responds with app.get
  console.log('Express server is running on localhost:3001')
);
